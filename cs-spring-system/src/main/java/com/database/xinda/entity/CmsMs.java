package com.database.xinda.entity;

import lombok.Data;

/**
 * @program: cs-spring-cloud -> com.demo.entity
 * @description:
 * @author: XinDa2020
 * @create: 2021/6/1 14:14:47
 * @Version 1.0
 **/
@Data
public class CmsMs {

    private String id;

    private String msName;


}
