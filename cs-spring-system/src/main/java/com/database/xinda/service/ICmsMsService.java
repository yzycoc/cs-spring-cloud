package com.database.xinda.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.database.xinda.entity.CmsMs;

/**
 * @program: cs-spring-cloud -> com.demo.service
 * @description:
 * @author: XinDa2020
 * @create: 2021/6/1 14:14:50
 * @Version 1.0
 **/
public interface ICmsMsService extends IService<CmsMs> {
}
