package com.database.poi.service.impl;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.database.poi.service.IPoiQrCodeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Hashtable;

/**
 * @program: cs-spring-cloud -> com.database.poi.service.impl
 * @description:
 * @author: XinDa2020
 * @create: 2021/7/20 11:11:11
 * @Version 1.0
 **/
@Slf4j
@Service
public class PoiQrCodeServiceImpl  implements IPoiQrCodeService {


    @Override
    public void poiDemo(HttpServletResponse response) {

        OutputStream fileOut = null;
        try {
            /***数据处理 -> 生成二维码 并且转换成字节码 ***/
            BufferedImage qrCode = createImage("夏：二维码扫码内容");
            ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
            ImageIO.write(qrCode, "png", byteArrayOut);
            /** 基本的POI数据处理 */
            // 生成工作簿
            XSSFWorkbook wb = new XSSFWorkbook();
            // 创建sheet页
            XSSFSheet sheet =  wb.createSheet("sheet的名称");
            //获取第一行 即 A
            XSSFRow r0 = sheet.createRow(0);
            //获取第一行 即 A1 第一列
            XSSFCell r0c0 = r0.createCell(0);
            //往 A1 写入数据
            r0c0.setCellValue("数据1：夏雨辰");

            /** 插入图片 **/
            XSSFDrawing patriarch = (XSSFDrawing) sheet.createDrawingPatriarch();
            XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0,(short) 1, 1, (short) 2, 0);
            patriarch.createPicture(anchor, wb.addPicture(byteArrayOut.toByteArray(), HSSFWorkbook.PICTURE_TYPE_JPEG));
            /** 返回给前端字节流文件 */
            fileOut = response.getOutputStream();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=demo.xlsx");
            wb.write(fileOut);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /***
     * 生成二维码
     * @param content
     * @return
     */
    public static BufferedImage createImage(String content){
        try {
            Hashtable hints = new Hashtable();
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            hints.put(EncodeHintType.MARGIN, 1);
            BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, 300, 300,
                    hints);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000 : 0xFFFFFFFF);
                }
            }
            // 插入图片
            return image;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
