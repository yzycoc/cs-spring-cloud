package com.database.poi.service;

import javax.servlet.http.HttpServletResponse;

/**
 * @program: cs-spring-cloud -> com.database.poi.service
 * @description:
 * @author: XinDa2020
 * @create: 2021/7/20 11:11:10
 * @Version 1.0
 **/
public interface IPoiQrCodeService {
    void poiDemo(HttpServletResponse response);
}
