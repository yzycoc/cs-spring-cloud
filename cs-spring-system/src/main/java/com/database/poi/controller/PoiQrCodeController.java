package com.database.poi.controller;

import com.database.poi.service.IPoiQrCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * @program: cs-spring-cloud -> com.yzycoc.poidemo.controller
 * @description: POIdemo
 * @author: XinDa2020
 * @create: 2021/7/20 10:10:49
 * @Version 1.0
 **/
@Api(tags="POI DEMO")
@Slf4j
@Controller
@RequestMapping("/poi")
public class PoiQrCodeController {


    @Autowired
    private IPoiQrCodeService iPoiQrCodeService;


    @ApiOperation("文件导出测试")
    @GetMapping(value = "/poiDemo")
    public void poiDemo(HttpServletResponse response){
        iPoiQrCodeService.poiDemo(response);
    }


    @ApiOperation("返回请求接口")
    @GetMapping(value = "/poiUrl")
    @ResponseBody
    public String poiUrl(){
        return "/poiDemo";
    }
}
