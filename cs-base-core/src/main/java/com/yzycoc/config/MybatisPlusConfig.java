package com.yzycoc.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;


/**
 * @program: cs-spring-cloud -> com.yzycoc.config
 * @description:
 * @author: XinDa2020
 * @create: 2021/6/1 10:10:21
 * @Version 1.0
 **/
@Configuration
@MapperScan(value={"com.database.**.mapper*"})
public class MybatisPlusConfig {

}
