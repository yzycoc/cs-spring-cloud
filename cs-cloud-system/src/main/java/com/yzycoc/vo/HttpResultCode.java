package com.yzycoc.vo;

/**
 * @program: cs-spring-cloud -> cs.yzycoc.common
 * @description: HTTP返回代码
 * @author: XinDa2020
 * @create: 2021/6/1 15:15:28
 * @Version 1.0
 **/
public class HttpResultCode {
    /** 请求者应当继续提出请求。服务器返回此代码表示已收到请求的第一部分，正在等待其余部分。 */
    public static final Integer CODE_100 = 100;
    /** (切换协议)请求者已要求服务器切换协议，服务器已确认并准备切换。 */
    public static final Integer CODE_101 = 101;
    /**(成功)服务器已成功处理了请求。 */
    public static final Integer CODE_200 = 200;
    /**(已创建)请求成功并且服务器创建了新的资源。 */
    public static final Integer CODE_201 = 201;
    /**(已接受)服务器已接受请求，但尚未处理。 */
    public static final Integer CODE_202 = 202;
    /**(非授权信息)服务器已成功处理了请求，但返回的信息可能来自另一来源。 */
    public static final Integer CODE_203 = 203;
    /**(无内容)服务器成功处理了请求，但没有返回任何内容。 */
    public static final Integer CODE_204 = 204;
    /**(重置内容)服务器成功处理了请求，但没有返回任何内容。 */
    public static final Integer CODE_205 = 205;
    /**(部分内容)服务器成功处理了部分 GET 请求。 */
    public static final Integer CODE_206 = 206;

    /**(错误请求)服务器不理解请求的语法。 */
    public static final Integer CODE_400 = 400;
    /**(未授权)请求要求身份验证。对于登录后请求的网页，服务器可能返回此响应。 */
    public static final Integer CODE_401 = 401;
    /**(禁止)服务器拒绝请求。 */
    public static final Integer CODE_403 = 403;
    /**(未找到)服务器找不到请求的网页。例如，对于服务器上不存在的网页经常会返回此代码。 */
    public static final Integer CODE_404 = 404;
    /**(方法禁用)禁用请求中指定的方法。 */
    public static final Integer CODE_405 = 405;
    /**(不接受)无法使用请求的内容特性响应请求的网页。 */
    public static final Integer CODE_406 = 406;
    /**(需要代理授权)此状态码与 401(未授权)类似，但指定请求者应当授权使用代理。 */
    public static final Integer CODE_407 = 407;
    /**(请求超时)服务器等候请求时发生超时。 */
    public static final Integer CODE_408 = 408;
    /**(冲突)服务器在完成请求时发生冲突。服务器必须在响应中包含有关冲突的信息。 */
    public static final Integer CODE_409 = 409;
    /**(已删除)如果请求的资源已永久删除，服务器就会返回此响应。 */
    public static final Integer CODE_410 = 410;
    /**(需要有效长度)服务器不接受不含有效内容长度标头字段的请求。 */
    public static final Integer CODE_411 = 411;
    /**(未满足前提条件)服务器未满足请求者在请求中设置的其中一个前提条件。 */
    public static final Integer CODE_412 = 412;
    /**(请求实体过大)服务器无法处理请求，因为请求实体过大，超出服务器的处理能力。 */
    public static final Integer CODE_413 = 413;
    /**(请求的 URI 过长)请求的 URI(通常为网址)过长，服务器无法处理。 */
    public static final Integer CODE_414 = 414;
    /**(不支持的媒体类型)请求的格式不受请求页面的支持。 */
    public static final Integer CODE_415 = 415;
    /**(请求范围不符合要求)如果页面无法提供请求的范围，则服务器会返回此状态码。 */
    public static final Integer CODE_416 = 416;
    /**(未满足期望值)服务器未满足”期望”请求标头字段的要求。 */
    public static final Integer CODE_417 = 417;

    /**(服务器内部错误)服务器遇到错误，无法完成请求。 */
    public static final Integer CODE_500 = 500;
    /**(尚未实施)服务器不具备完成请求的功能。 */
    public static final Integer CODE_501 = 501;
    /**(错误网关)服务器作为网关或代理，从上游服务器收到无效响应. */
    public static final Integer CODE_502 = 502;
    /**(服务不可用)服务器目前无法使用(由于超载或停机维护)。通常，这只是暂时状态。 */
    public static final Integer CODE_503 = 503;
    /**(网关超时)服务器作为网关或代理，但是没有及时从上游服务器收到请求。*/
    public static final Integer CODE_504 = 504;
    /**(HTTP 版本不受支持)服务器不支持请求中所用的 HTTP 协议版本。 */
    public static final Integer CODE_505 = 505;
    /**访问权限认证未通过 510 AUTHZ */
    public static final Integer CODE_510 = 510;




}
