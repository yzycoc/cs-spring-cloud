package com.yzycoc.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @program: cs-spring-cloud -> cs.yzycoc.common.api.vo
 * @description: 接口返回数据格式
 * @author: XinDa2020
 * @create: 2021/6/1 14:14:54
 * @Version 1.0
 * AllArgsConstructor 全参
 * NoArgsConstructor 空参
 *
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value="接口返回对象", description="接口返回对象")
public class Result<T> implements Serializable {
    /**显示声明serialVersionUID可以避免对象不一致 分布式 可能用得到 */
    private static final long serialVersionUID = 1L;
    /**
     * 成功标志
     */
    @ApiModelProperty(value = "成功标志")
    private boolean success = true;

    /**
     * 返回处理消息
     */
    @ApiModelProperty(value = "返回处理消息")
    private String message = "操作成功！";

    /**
     * 返回代码
     */
    @ApiModelProperty(value = "返回代码")
    private Integer code = 0;

    /**
     * 返回数据对象 data
     */
    @ApiModelProperty(value = "返回数据对象")
    private T result;

    /**
     * 时间戳
     */
    @ApiModelProperty(value = "时间戳")
    private long timestamp = System.currentTimeMillis();

    /*****
     * 非静态类方法
     * ***/
    public Result<T> success(String message) {
        this.message = message;
        this.code = HttpResultCode.CODE_200;
        this.success = true;
        return this;
    }


    public Result<T> error500(String message) {
        this.message = message;
        this.code = HttpResultCode.CODE_500;
        this.success = false;
        return this;
    }

    public Result<T> ok200(String message) {
        this.message = message;
        this.code = HttpResultCode.CODE_200;
        this.success = true;
        return this;
    }

    public Result<T> ok200(T data) {
        this.result = data;
        this.code = HttpResultCode.CODE_200;
        this.success = true;
        return this;
    }
    public Result<T> ok200(String message,T data) {
        this.result = data;
        this.code = HttpResultCode.CODE_200;
        this.success = true;
        return this;
    }

    /**  测试调用删除的内容 **/
    @Deprecated
    public static Result<Object> ok(Object data) {
        Result<Object> r = new Result<Object>();
        r.setSuccess(true);
        r.setCode(HttpResultCode.CODE_200);
        r.setResult(data);
        return r;
    }
    /*****
     * 静态类调用的方法
     * ***/
    /*****    正常   ****/

    public static<T> Result<T> OK() {
        Result<T> r = new Result<T>();
        r.setSuccess(true);
        r.setCode(HttpResultCode.CODE_200);
        r.setMessage("成功");
        return r;
    }

    public static<T> Result<T> OK(T data) {
        Result<T> r = new Result<T>();
        r.setSuccess(true);
        r.setCode(HttpResultCode.CODE_200);
        r.setResult(data);
        return r;
    }

    public static<T> Result<T> OK(String msg, T data) {
        Result<T> r = new Result<T>();
        r.setSuccess(true);
        r.setCode(HttpResultCode.CODE_200);
        r.setMessage(msg);
        r.setResult(data);
        return r;
    }

    /*****    异常   ****/
    public static Result<Object> error(String msg) {
        return error(HttpResultCode.CODE_500, msg);
    }

    public static Result<Object> error(int code, String msg) {
        Result<Object> r = new Result<Object>();
        r.setCode(code);
        r.setMessage(msg);
        r.setSuccess(false);
        return r;
    }

    /** JsonIgnore 注解： java bean 属性忽略掉 */
    @JsonIgnore
    private String onlTable;
}
