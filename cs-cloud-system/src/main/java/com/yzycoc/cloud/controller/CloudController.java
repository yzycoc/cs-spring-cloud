package com.yzycoc.cloud.controller;

import com.yzycoc.vo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @program: cs-spring-cloud -> com.yzycoc.cloud.controller
 * @description:
 * @author: XinDa2020
 * @create: 2021/6/24 09:9:28
 * @Version 1.0
 **/
@Api(tags="单元测试 模块：cs-cloud-system")
@RestController
@RequestMapping("/demo/system/cloud")
public class CloudController {

    public static final String URL = "http://CS-CLOUD";
    //public static final String URL = "http://127.0.0.1:8880";
    @Autowired
    private RestTemplate restTemplate;

    @ApiOperation("测试接口")
    @GetMapping(value = "/isok")
    public Result<?> isOk() {
        return restTemplate.getForObject(URL + "/cs/demo/cs/cloud/isok",Result.class);
    }


    @ApiOperation("测试接口no")
    @GetMapping(value = "/isno")
    public Result<?> isNo() {
        return restTemplate.getForObject(URL + "/cs/demo/cs/cloud/isno",Result.class);
    }


    @ApiOperation("设备列表")
    @GetMapping(value = "/cms")
    public Result<?> cms() {
        return restTemplate.getForObject(URL + "/cs/demo/cs/cloud/msList",Result.class);
    }
}
