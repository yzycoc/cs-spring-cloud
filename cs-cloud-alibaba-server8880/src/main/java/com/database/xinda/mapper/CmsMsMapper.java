package com.database.xinda.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.database.xinda.entity.CmsMs;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @program: cs-spring-cloud -> com.demo.mapper
 * @description: 测试
 * @author: XinDa2020
 * @create: 2021/6/1 14:14:48
 * @Version 1.0
 **/
@Mapper
@Component
public interface CmsMsMapper extends BaseMapper<CmsMs> {

}
