package com.database.xinda.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.database.xinda.entity.CmsMs;
import com.database.xinda.mapper.CmsMsMapper;
import com.database.xinda.service.ICmsMsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
/**
 * @program: cs-spring-cloud -> com.demo.service.impl
 * @description:
 * @author: XinDa2020
 * @create: 2021/6/1 14:14:50
 * @Version 1.0
 **/
@Slf4j
@Service(value = "ICmsMsServices")
@Primary
public class CmsMsServiceImpl extends ServiceImpl<CmsMsMapper, CmsMs> implements ICmsMsService {
}
