package com.demo.test.reflect.dev20211209;

/**
 * @program: cs-spring-cloud -> com.demo.test.reflect.dev20211209
 * @description:
 * @author: 会跑的仓鼠
 * @create: 2021/12/9 14:14:30
 * @Version 1.0
 **/
public class Test1430 {
    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> aClass = Class.forName("com.demo.test.reflect.dev20211209.Student");
        System.out.println(aClass.hashCode());

        User user = new Student();
        System.out.println(user.getClass().hashCode());
    }

}
class User{
    public String vio;

    public String getVio() {
        return vio;
    }

    public void setVio(String vio) {
        this.vio = vio;
    }

    public User() {
    }

    public User(String vio) {
        this.vio = vio;
    }
}

class Student extends User{
     public Student(){
         this.vio  = "名称";
     }
}

class Teacher extends User{
    public Teacher(){
        this.vio = "nihao";
    }
}