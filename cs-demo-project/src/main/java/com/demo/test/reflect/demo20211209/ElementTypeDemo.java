package com.demo.test.reflect.demo20211209;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @program: cs-spring-cloud -> PACKAGE_NAME
 * @description:
 * @author: 会跑的仓鼠
 * @create: 2021/12/9 10:10:00
 * @Version 1.0
 **/
@MyInter(date = 1)
@MyInter2(value = myenum.DEMO,del = {myenum.DEMO,myenum.VAL})
public class ElementTypeDemo {

}

/****
 * 内部 注解类
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.CLASS)
@interface MyInter{
    String value() default "默认值";
    int age() default 1;
    int date();
}

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.CLASS)
@interface MyInter2{
    myenum value();
    myenum[] del();
}
enum myenum{
    VAL,
    DEMO,
    TEST
}