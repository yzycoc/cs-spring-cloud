package com.demo.test.asserts;

/**
 * @program: cs-spring-cloud -> com.demo.test.asserts
 * @description: 断言
 * @author: 会跑的仓鼠
 * @create: 2021/12/9 12:12:41
 * @Version 1.0
 **/

public class AssertsDemo {

    public static void main(String[] args) {
        test(0);

    }

    private static void test(int i) {
        assert i>=10:"请输入大于10的内容";
        System.out.println(i);
    }
}
