package com.demo.test.phone;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;

import java.util.Map;

/**
 * @program: cs-spring-cloud -> PACKAGE_NAME
 * @description:
 * @author: XinDa2020
 * @create: 2021/8/19 18:18:52
 * @Version 1.0
 **/
public class PhoneDemo {


    /**
     * 使用AK&SK初始化账号Client
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new com.aliyun.dysmsapi20170525.Client(config);
    }

    public static void main(String[] args_) throws Exception {
        java.util.List<String> args = java.util.Arrays.asList(args_);
        com.aliyun.dysmsapi20170525.Client client = PhoneDemo.createClient("LTAI5tRKJvkky71Fxujthc5o", "lKB8fDoCBfJNx8Cqkoy89hHpOd5GB9");
        JSONObject param = new JSONObject();
        param.put("msId","设备1");
        param.put("localhost","设备1");
        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setPhoneNumbers("18871733824")
                .setSignName("仓鼠")
                .setTemplateCode("SMS_222460392")
                .setTemplateParam(param.toString());        // 复制代码运行请自行打印 API 的返回值
        SendSmsResponse sendSmsResponse = client.sendSms(sendSmsRequest);
        Map<String, Object> stringObjectMap = sendSmsRequest.toMap();
        if(stringObjectMap != null){
            for (String s : stringObjectMap.keySet()) {
                System.out.println(s);
            }
        }


        System.out.println(sendSmsResponse.getBody().getCode());
        System.out.println(sendSmsResponse.getBody().getMessage());
        System.out.println(sendSmsResponse.getBody().getRequestId());
        for (String s : sendSmsResponse.body.toMap().keySet()) {
            System.out.println(s+" : "+sendSmsResponse.body.toMap().get(s));
        }
        System.out.println("-----------------");
        for (String s : sendSmsResponse.headers.keySet()) {
            System.out.println(s+" : "+sendSmsResponse.headers.get(s));
        }

    }

    /****
     *
     */
    public static void toMail(){

    }
}
