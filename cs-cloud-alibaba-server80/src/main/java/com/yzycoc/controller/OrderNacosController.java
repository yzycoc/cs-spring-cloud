package com.yzycoc.controller;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.listener.Event;
import com.alibaba.nacos.api.naming.listener.EventListener;
import com.alibaba.nacos.api.naming.listener.NamingEvent;
import com.yzycoc.common.api.vo.Result;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Hashtable;
import java.util.Properties;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.listener.Event;
import com.alibaba.nacos.api.naming.listener.EventListener;
import com.alibaba.nacos.api.naming.listener.NamingEvent;
import javax.annotation.Resource;
import java.util.Properties;

/**
 * @program: cscocutil
 * @description:
 * @author: yao
 * @create: 2021-12-12 21:15
 * @Version 1.0
 **/
@RestController
@Slf4j
@RefreshScope
public class OrderNacosController
{
    @Resource
    private RestTemplate restTemplate;

    @Value("${service-url.nacos-user-service}")
    private String serverURL;

    @ApiOperation("A测试接口")
    @GetMapping(value = "/isok")
    public Result<?> isOk() {
        log.info(serverURL + "/cs/demo/cs/cloud/isok");

        return restTemplate.getForObject(serverURL + "/cs/demo/cs/cloud/isok",Result.class);
    }


    @ApiOperation("测试接口no")
    @GetMapping(value = "/isno")
    public Result<?> isNo() {
        return restTemplate.getForObject(serverURL + "/cs/demo/cs/cloud/isno",Result.class);
    }


    @ApiOperation("设备列表")
    @GetMapping(value = "/cms")
    public Result<?> cms() {
        return restTemplate.getForObject(serverURL + "/cs/demo/cs/cloud/msList",Result.class);
    }

    @ApiOperation("B测试接口")
    @GetMapping(value = "/isBok")
    public Result<?> isBOk() throws NacosException {
        return restTemplate.getForObject(serverURL + "/cs/demo/cs/cloud/isok",Result.class);
    }

}
