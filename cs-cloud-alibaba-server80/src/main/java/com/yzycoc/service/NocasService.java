package com.yzycoc.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @program: cs-spring-cloud -> com.yzycoc
 * @description:
 * @author: 会跑的仓鼠
 * @create: 2021/12/13 13:13:42
 * @Version 1.0
 **/
//@FeignClient(name = "service-provider")
public interface NocasService {
    @GetMapping(value = "/echo/{str}")
    String echo(@PathVariable("str") String str);
}
