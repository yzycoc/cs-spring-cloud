package com.yzycoc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @program: cscocutil
 * @description:
 * @author: yao
 * @create: 2021-12-12 20:58
 * @Version 1.0
 **/
@Slf4j
@SpringBootApplication
@EnableDiscoveryClient
public class Cs80Aplicatopn {
    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application = SpringApplication.run(Cs80Aplicatopn.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path");
        if(path == null){
            path = "";
        }
        log.info("\n----------------------------------------------------------\n\t" +
                "Application cs-spring-cloud is running! Access URLs:\n\t" +
                "Local: \t\thttp://localhost:" + port + path + "/\n\t" +
                "External: \thttp://" + ip + ":" + port + path + "/\n\t" +
                "Swagger文档: \thttp://" + ip + ":" + port + path + "/doc.html\n" +
                "----------------------------------------------------------");

    }

    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate(){
        System.out.println("rest配置");
        return new RestTemplate();
    }
}
